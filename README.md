# README #

### What is this repository for? ###

- Beginners Guide to a Simple Django Project for Student Social Media Management Portal

### How do I get set up? ###

1. Install Python 2.7 and Django 1.10.4
2. MySQL Database - Configuration is in settings.py File
3. python manage.py runserver to deploy it

### Contribution guidelines ###

- It is a beginners which will help me to learn Django better
- You may contribute