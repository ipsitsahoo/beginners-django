from models import Registered, University
from django.db import DatabaseError
import pytz
from django.utils import timezone
from django.core.validators import validate_email
from django.core.exceptions import ValidationError


def validate_mail(email):
    try:
        validate_email(email)
        return True
    except ValidationError:
        return False


def create_dt():
    timezone.activate(pytz.timezone("Asia/Kolkata"))
    return timezone.localtime(timezone.now())


def validate_data(mail, key):
    response = {}
    try:
        check_with_db = Registered.objects.get(email_id=mail, password=key)
        print(type(check_with_db))
        if check_with_db.email_id == mail:
            response["id"] = 200
            response["msg"] = "Valid Credentials"
            response["date_of_creation"] = check_with_db.timestamp
        else:
            response["id"] = 300
            response["msg"] = "Invalid Credentials"
    except Registered.DoesNotExist:
        response["id"] = 404
        response["msg"] = "Email address or password is incorrect"
    return response


def insert_data(email, first_n, last_n, pwd, univ):
    v = Registered.objects.all()
    t = len(v)
    univ_details = University.objects.get(univ_name=univ)
    date_time = create_dt()
    try:
        b = Registered(user_id=(t + 1), email_id=email, first_name=first_n, last_name=last_n, password=pwd,
                       timestamp=date_time, university=univ_details)
        b.save()
        return 1
    except DatabaseError as e:
        print(e)
        return 0


def get_university():
    univs = University.objects.all()
    list_us = []
    for univ in univs:
        list_us.append(univ.univ_name)
    return list_us


def get_user_data(value):
    data = Registered.objects.get(email_id=value)
    return data
