from __future__ import unicode_literals

from django.db import models


class University(models.Model):
    univ_name = models.CharField(max_length=1000, default="Not Specified")
    univ_city = models.CharField(max_length=1000, default="Not Specified")
    univ_state = models.CharField(max_length=1000, default="Not Specified")
    univ_country = models.CharField(max_length=1000, default="Not Specified")
    timestamp = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.univ_name + " " + self.univ_city


class Registered(models.Model):
    user_id = models.AutoField(unique=True, primary_key=True)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    email_id = models.CharField(max_length=200)
    password = models.CharField(max_length=200, default="Mumbai_Indians")
    timestamp = models.DateTimeField(blank=True, null=True)
    university = models.ForeignKey(University, on_delete=models.CASCADE, db_constraint=False)

    class Meta:
        unique_together = ('user_id', 'email_id')

    def __str__(self):
        return self.email_id
