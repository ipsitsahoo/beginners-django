/**
 * Created by shuttle3468 on 21/5/17.
 */
$(document).ready(function () {
    var form_control = ".form-con";
    $(form_control).find("input").keydown(function (event) {
        $(this).siblings("label").addClass("shift-left");
    });
    $(form_control).find("input").keyup(function (event) {
        if ($(this).val().length === 0) {
            $(this).siblings("label").removeClass("shift-left");
        }
    });
});

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
function register_user() {
    var f_name = $("[name='fname']").val();
    var l_name = $("[name='lname']").val();
    var email = $("[name='mail']").val();
    var pass = $("[name='pass']").val();
    var university = $(".university").val();
    var csrfToken = getCookie('csrftoken');
    console.log(csrfToken);
    $.ajax({
        url: '/register/',
        type: 'POST',
        dataType: 'json',
        data: {
            csrfmiddlewaretoken: csrfToken,
            first_name: f_name,
            last_name: l_name,
            email: email,
            pass: pass,
            univ: university
        },
        beforeSend: function () {
            $("#register").attr("value", "REGISTERING ...");
        },
        success: function (response) {
            if (response.id === 200) {
                $("#register").attr("value", "REGISTERED");
                push_notifs(response.id, response.msg);
            }
            else {
                $("#register").attr("value", "ERROR");
                push_notifs(response.id, response.msg);
            }
            console.log(response);
        },
        error: function () {
            $("#register").attr("value", "FAILED");
            push_notifs(400, "Unexpected Connection Error");
        }
    });
}

function login_user() {
    var user_mail = $("[name='user-mail']").val();
    var user_pass = $("[name='user-pass']").val();
    var csrfToken = getCookie('csrftoken');
    console.log(csrfToken);
    $.ajax({
        url: '/login/',
        type: 'POST',
        dataType: 'json',
        data: {csrfmiddlewaretoken: csrfToken, auth_mail: user_mail, auth_pass: user_pass},
        beforeSend: function () {
            $("#login").attr("value", "LOGGING YOU IN ...");
        },
        success: function (response) {
            if (response.id === 200) {
                $("#login").attr("value", "LOGGED IN");
                window.setTimeout(function () {
                    window.location = "/dashboard/";
                }, 1000);
            }
            else {
                $("#login").attr("value", "LOG IN ERROR");
                push_notifs(response.id, response.msg);
            }
        },
        error: function () {
            push_notifs(400, "Failed to Connect");
            $("#login").attr("value", "FAILED");
        }
    });
}
function push_notifs(id, message) {
    var box = $("#notif-box");
    $(box).addClass("show-notification");
    if (id === 200) {
        $(box).text(message);
        $(box).removeClass("show-failure").addClass("show-success");
    }
    else {
        $(box).text(message);
        $(box).removeClass("show-success").addClass("show-failure");
    }
    setTimeout(function () {
        var notifs = $("#notif-box");
        if(notifs.hasClass("show-notification")) {
            notifs.removeClass("show-notification");
        }
    }, 5000);
}