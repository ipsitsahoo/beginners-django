from django.http import JsonResponse
from django.shortcuts import render, redirect
from functions import *
# Create your views here.


def index(request):
    list_d = get_university()
    context = {'University_Name': list_d}
    return render(request, 'index.html', context)


def dashboard(request):
    values = get_user_data(request.session["user_id"])
    print(values)
    if values:
        context = {'request': request, 'user_data': values}
    else:
        context = {'request': request}
    return render(request, 'dashboard.html', context)


def register_user(request):
    response = {}
    if request.method == "POST":
        first_name = request.POST.get("first_name")
        last_name = request.POST.get("last_name")
        email = request.POST.get("email")
        password = request.POST.get("pass")
        university = request.POST.get("univ")
        if not validate_mail(email):
            response["id"] = 300
            response["msg"] = "Email Format Entered was incorrect"
        res = insert_data(email, first_name, last_name, password, university)
        if len(first_name) == 0 or len(last_name) == 0 or len(email) == 0 or len(password) == 0:
            response["id"] = 300
            response["msg"] = "One or elements are unfilled"
        elif res == 0:
            response["id"] = 404
            response["msg"] = "We Could not add your data to the DB. Please make sure that correct data is entered"
        elif res == 1:
            response["id"] = 200
            response["msg"] = "Successfully Received"
            print(email)
        else:
            response["id"] = 500
            response["msg"] = "An Unknown Error Occurred"
    return JsonResponse(response)


def login_user(request):
    response = {}
    if request.method == "POST":
        email_id = request.POST.get("auth_mail")
        password = request.POST.get("auth_pass")
        if not validate_mail(email_id):
            response["id"] = 300
            response["msg"] = "Email format entered was incorrect"
        elif len(email_id) == 0 or len(password) == 0:
            response["id"] = 300
            response["msg"] = "One or elements are unfilled"
        else:
            response = validate_data(email_id, password)
        print("Got this in Login Form" + email_id)
        if response["id"] == 200:
            request.session["user_id"] = email_id
    return JsonResponse(response)


def logout(request):
    del request.session["user_id"]
    request.session.flush()
    return redirect('/auth/')
